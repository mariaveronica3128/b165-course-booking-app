/*
	We will create our own module which will have methods that will help us authorize our users to access or to disallow access to certain parts/ features in our app
*/

// imports
const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

//Token Creation
module.exports.createAccessToken = (user) => {
	console.log(user)

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {})

};
//Verification
module.exports.verify = (req, res, next) => {
    console.log(req.headers.authorization);
    let token = req.headers.authorization;

    if(typeof token === "undefined"){
        return res.send({auth: "Failed. No Token"})
    } else {
        console.log(token);
        token = token.slice(7, token.length);
        console.log(token);
        jwt.verify(token, secret, function(err, decodedToken){
            if(err){
                return res.send({
                    auth: "Failed",
                    message: err.message
                });
            } else {
                console.log(decodedToken);
                req.user = decodedToken;

                next();
            }
        })
    }
};
module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}
