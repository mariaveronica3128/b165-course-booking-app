//[SECTION] Packages and Dependencies
const express = require("express"); 
const mongoose = require("mongoose");  
const dotenv = require("dotenv");  
const courseRoutes = require('./routes/courses'); 
const userRoutes = require('./routes/users')

//[SECTION] Server Setup
const apps = express(); 
dotenv.config(); 
apps.use(express.json());
const secret = process.env.CONNECTION_STRING;
const port = process.env.PORT; 

//[SECTION] Application Routes
apps.use('/courses', courseRoutes); 
apps.use('/users', userRoutes)

//[SECTION] Database Connection
mongoose.connect(secret)
let connectStatus = mongoose.connection; 
connectStatus.on('open', () => console.log('Database is Connected'));

//[SECTION] Gateway Response
apps.get('/', (req, res) => {
   res.send(`Welcome to FIRST NAME/s CUSTOM NAME APP`); 
}); 
apps.listen(port, () => console.log(`Server is running on port ${port}`)); 
