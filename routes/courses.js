//[SECTION] Dependencies and Modules
const exp = require("express");
const { route } = require("express/lib/application");
const controller = require('../controllers/courses');
const auth = require('../auth')

const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const routes = exp.Router(); 

//[SECTION] [POST] Routes

 routes.post('/create', verify, verifyAdmin, (req, res) => { 
     let data = req.body; 
     controller.createCourse(data).then(outcome => {
            res.send(outcome); 
     });
 });

//[SECTION] [GET] Routes
 
 routes.get('/all', (req, res) => {
    controller.getAllCourse().then(outcome => {
      res.send(outcome);
    });
 });

routes.get('/:id', (req, res) => {

  let courseId = req.params.id;

  controller.getCourse(courseId).then(result =>{
    res.send(result);
  })
});

routes.get('/', (req, res) =>{

  controller.getAllActiveCourse().then(outcome => {
    res.send(outcome);
  });
});


//[SECTION] [PUT] Routes
routes.put('/:id',(req,res) => {
  let id = req.params.id;
  let details= req.body;

  let cName = details.name;
  let cDesc = details.description;
  let cPrice = details.price;
  
  if (cName !== ''&& cDesc !== '' && cPrice !==''){
      controller.updateCourse(id,details).then(outcome =>{
          res.send(outcome)
      })
  }else{
          res.send(`Some date is mising, please check again`)
  }
 
  
});

routes.put('/:id/archive',(req,res) => {
  let courseId = req.params.id;
  //console.log(courseId);

  controller.deactivateCourse(courseId).then(result => {
      res.send(result);
  });
});


routes.put('/:id/reactivate',(req,res) => {
  let courseId = req.params.id;

  controller.reactivateCourse(courseId).then(result => {
      res.send(result);
  });
});

//[SECTION] [DEL] Routes
routes.delete('/:id', (req, res) => {
  let id = req.params.id;

  controller.deleteCourse(id).then(outcome => {
    res.send(outcome);
  })
});


//[SECTION] Export Route System
module.exports = routes; 
