//[SECTION] Dependencies and Modules
const { get } = require('express/lib/response');
const { findByIdAndRemove } = require('../models/Course');
const Course = require('../models/Course');

//[SECTION] Functionality [CREATE]
   module.exports.createCourse = (info) => {
     let cName = info.name;
     let cDesc = info.description;
     let cCost = info.price;
     let newCourse = new Course({
     	name: cName,
     	description: cDesc,
     	price: cCost
     }) 
   	 return newCourse.save().then((savedCourse, error) => {
   	 	if (error) {
   	 		return 'Failed to Save New Document';
   	 	} else {
   	 		return savedCourse; 
   	 	}
   	 });
   };
 module.exports.getAllCourse = () => {
   
     return Course.find({}).then(outcomeNiFind => {
        return outcomeNiFind;
     });
   };

   module.exports.getCourse = (id) => {
	   return Course.findById(id).then(resultOfQuery => 
		{
			return resultOfQuery;
		})
   };

   module.exports.getAllActiveCourse = () =>{

   return Course.find({isActive: true}).then(resultOfQuery => {
      return resultOfQuery;

   });
   };

//[SECTION] Functionality [UPDATE]

   module.exports.updateCourse = (id,details) => {
   //req first
   let cName = details.name;
   let cDesc = details.description;
   let cPrice = details.price;

   let updatedCourse = {
      name: cName,
      description:cDesc,
      price: cPrice
   }

   return Course.findByIdAndUpdate(id, updatedCourse).then((
      courseUpdated, err)=>{
      if (err){
         return 'Failed to update course';
      }

      else {
         return 'Sucessfully updated course';
      }

   })
   };

   module.exports.deactivateCourse = (id) =>{
      let updates = {
      isActive: false
      }
      return Course.findByIdAndUpdate(id,updates).then((archived, error) =>{
         if (archived) {
            return `The course ${id} has been deactivated`
         }
         else {
            return `Failed to archive course`
         }
      });
   };

   module.exports.reactivateCourse = (id) =>{
      let updates = {
            isActive: true
      }
      return Course.findByIdAndUpdate(id,updates).then((reactivate, error) => {
         if (reactivate) {
            return `The course ${id} has been reactivated`
         }
         else {
            return `Failed to activate course`
         }
      });
   };

//[SECTION] Functionality [DELETE]

   module.exports.deleteCourse= (id) => {
 
   return Course.findByIdAndRemove(id).then((
      removedCourse, err) => {
   if (err) {
      return 'No course was removed';
   }
   else{
      return 'A course successfully deleted';
   };
   });
   };