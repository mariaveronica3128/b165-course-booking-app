//[SECTION] Dependencies and Modules
  const User = require('../models/User');
  const auth = require('../auth') 

  const bcrypt = require("bcrypt");
  const dotenv = require("dotenv");
  const req = require('express/lib/request');
  const Course = require('../models/Course');


//[SECTION] Environment Setup
  dotenv.config();
  const salt = parseInt(process.env.SALT);


//[SECTION] Functionalities [CREATE]
 
  module.exports.registerUser = (data) => {


    let fName= data.fistName;
    let lName= data.lastName;
    let email= data.email;
    let passW= data.password;
    let gendr= data.gender;
    let mobil= data.mobileNo;

    let newUser = new User({
      firstName: fName,
      lastName: lName,
      email: email,
      password: bcrypt.hashSync(passW,salt),
      gender: gendr,
      mobileNo: mobil

    });
  
  	return newUser.save().then((user,rejected) => {
      if (user) {
        return user; 
      }
      else{
        return 'Failed to register a new account';
      };
    });
  };

  module.exports.loginUser = (req, res) => {

      User.findOne({email: req.body.email}).then(foundUser => {
  
        if(foundUser === null){
          return res.send("User Not Found");
  
        } else {
  
          const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
  
          console.log(isPasswordCorrect)
  
          if(isPasswordCorrect){
  
            return res.send({accessToken: auth.createAccessToken(foundUser)})
  
          } else {
  
            return res.send("Incorrect Password")
          }
  
        }
  
      })
      .catch(err => res.send(err))
  
  };

  module.exports.getUserDetails = (req, res) => {
    console.log(req.user)
    User.findById(req.user.id).then(result => 
      res.send(result))
      .catch(err => res.send(err))
  };
  

module.exports.enroll = async (req, res) => {

  console.log(req.user.id)

  console.log(req.body.courseId)

if (req.user.isAdmin) {
  return res.send ("Action Forbidden")
}

let isUserUpdated = await User.findById(req.user.id).then(user => {

  let newEnrollment = {
    courseId: req.body.courseId
  }  

  user.enrollments.push(newEnrollment);
  return user.save().then(user => true).catch(err => err.message)
})

if(isUserUpdated != true){
  return res.send({message: isUserUpdated})
}


let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
  let enrollee ={
    userId: req.user.id
  }

  course.enrollees.push(enrollee);
  return course.save().then(user => true).catch(err => err.message)
})

if (isCourseUpdated !== true){
  return res.send({message: isCourseUpdated})
}

if(isUserUpdated && isCourseUpdated) {
  return res.send ({message: "Enrolled succcessfully."})
}
};

module.exports.getEnrollments = (req, res) => {
  User.findById(req.user.id).then(result => res.send(result.enrollments))
  .catch(err => res.send(err))
}

//[SECTION] Functionalities [RETRIEVE]
//[SECTION] Functionalities [UPDATE]
//[SECTION] Functionalities [DELETE]
